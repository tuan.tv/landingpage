$(document).ready(function() {
	// disable default event
	$('a').click(function(e) {
		e.preventDefault();
	})

	// get window size
	var $view = $( window ).width()

	// slide toggle menu mobile
	if($view < 768) {
		$('.header__nav').hide()
		$('.logo').click(function() {
			$('.header__nav').slideToggle(300)
		})
	}

	// add class active menu
	$('.header__menu li').click(function() {
		$(this).parent().find('.active').removeClass('active')
		$(this).addClass('active')
	})

	// slider section img
	var swiper_img = new Swiper('.section--img .swiper-container', {
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});

	// slider section customer
	var swiper_customer = new Swiper('.section--customer .swiper-container', {
		slidesPerView: 5,
		spaceBetween: 30,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});

	// slider section feature grey
	var $slidePreview = 3
	if($view < 768) {
		$slidePreview = 1
	}

	var swiper_customer = new Swiper('.section--feature-slide .swiper-container', {
		slidesPerView: $slidePreview,
		spaceBetween: 30,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		centeredSlides: true,
	});

	// slider section quote
	var swiper = new Swiper('.section--quote-slide .swiper-container', {
		slidesPerView: 1,
		spaceBetween: 30,
		loop: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
})